import tensorflow as tf
from skimage.transform import resize
from skimage.io import imread
import numpy as np
import math



def process_images(X, image_paths, target_dim):

    for i,path in enumerate(image_paths):
        x = imread(path)
        x = resize(x, target_dim)
        #x = scale(x, x_factor, y_factor)
        #x = translate(x, delta)
        #x = hsv_transform(x, factor)
        X[i,...] = x

        #TODO transformacije


def process_boxes(y, box_labels, d):


    for i, image_boxes in enumerate(box_labels):

        for box in image_boxes:
            # svi objekti u jednoj slici

            x_min = box['x_min']
            y_min = box['y_min']
            x_max = box['x_max']
            y_max = box['y_max']

            xc = (x_min + x_max) / 2.0
            yc = (y_min + y_max) / 2.0

            gx = int(xc / d)
            gy = int(yc / d)

            if np.any(y[i,gy,gx,:,4]==0):
                k = (y[i,gy,gx,:,4]==0).argmax()
                y[i,gy,gx,k,:] = np.array([x_min, y_min, x_max, y_max, 1.0])




class ImageDataGenerator(tf.keras.utils.Sequence):

    def __init__(self, labels, batch_size=32, image_size=(224,224,3), cell_size=7, boxes_per_cell=2, classes=0, shuffle=True):
        self.labels = labels
        self.batch_size = batch_size
        self.image_size = image_size
        self.cell_size = cell_size
        self.boxes_per_cell = boxes_per_cell
        self.classes = classes
        self.shuffle = shuffle
        self.indexes = np.arange(len(self.labels))
        self.on_epoch_end()


    def __len__(self):
        return math.ceil(len(self.labels) / self.batch_size)


    def __getitem__(self, idx):
        indexes = self.indexes[idx*self.batch_size:(idx+1)*self.batch_size]
        X,y = self.__data_generation(indexes)

        return X,y


    def on_epoch_end(self):
        if self.shuffle == True:
            np.random.shuffle(self.indexes)


    def __data_generation(self, indexes):

        d = self.image_size[0] / self.cell_size

        X = np.empty((self.batch_size, *self.image_size))

        image_paths = [self.labels[k]['path'] for k in indexes]
        process_images(X, image_paths, self.image_size)

        y = np.zeros((self.batch_size, self.cell_size, self.cell_size, 5, 5))

        box_labels = [self.labels[k]['boxes'] for k in indexes]
        process_boxes(y, box_labels, d)

        return X,y

