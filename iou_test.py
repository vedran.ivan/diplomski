import tensorflow as tf
import numpy as np

def get_cell_offsets(cell_size, s):
    x = np.arange(s)[np.newaxis,:]
    xx = np.tile(x.T, (1, s))
    yy = np.tile(x, (s,1))

    z = np.stack((xx, yy),axis=-1) * cell_size
    return z



def iou(a,b):
    """
    Calculate IOU (intersection over union), i.e. Jaccard overlap between two sets of boxes, A and B.
    Sets A and B can have different cardinalities.
    Uses top-left coordinate origin in image space.
    Uses [xmin, ymin, xmax, ymax] coordinate encoding.

    Args:
        a (np.ndarray, ndim <= 2): set A containing m x n  boxes
        b (np.ndarray, ndim <= 2): set B containing m x n x b boxes

    Returns (np.ndarray): m x n x b array.

    """
    #assert a.ndim in [1,2], "a must have at most 2 dimensions"
    #assert b.ndim in [1,2], "b must have at most 2 dimensions"
    assert b.shape[-1] == 4, "last dimension of a must have size of 4."
    assert a.shape[-1] == 4, "last dimension of a must have size of 4."

    if a.ndim == 1:
        a = a[np.newaxis,:]
    if b.ndim == 1:
        b = b[np.newaxis,:]

    aa = a[:,:,np.newaxis]
    #bb = b[np.newaxis]
    bb = b

    aa = np.tile(aa, (1, 1, b.shape[2], 1)) # m x n x b x 4


    Sa = (aa[...,2] - aa[...,0]) * (aa[...,3] - aa[...,1])
    Sb = (bb[...,2] - bb[...,0]) * (bb[...,3] - bb[...,1])

    ab_w = np.maximum(0, np.minimum(aa[..., 2], bb[..., 2]) - np.maximum(aa[..., 0], bb[..., 0]))
    ab_h = np.maximum(0, np.minimum(aa[..., 3], bb[..., 3]) - np.maximum(aa[..., 1], bb[..., 1]))

    aIb = ab_w * ab_h
    aUb = Sa + Sb - aIb

    iou = aIb / aUb

    return iou


#a = np.array([[0,0,4,4], [0,0,2,2]])
#b = np.array([[0,3,6,6], [0,0,5,7], [7,1,9,6]])


a = np.array([[[0,0,4,4], [0,0,2,2], [0,0,2,2]], [[0,0,2,2], [0,0,4,4], [0,0,2,2]]]) # 2 x 3 x 4
b = np.array([[[[0,3,6,6], [0,3,6,6], [7,1,9,6]],[[0,3,6,6], [0,3,6,6], [7,1,9,6]], [[0,3,6,6], [0,0,5,7], [0,0,5,7]]], [[[0,3,6,6], [0,0,5,7], [7,1,9,6]],[[0,3,6,6], [0,3,6,6], [7,1,9,6]], [[0,3,6,6], [0,0,5,7], [7,1,9,6]]]]) # 2 x 3 x b x 4

x = np.arange(2*1*2).reshape((2,1,2))
y = np.arange(2*2*2).reshape((2,2,2))

z = x[...,0] + y[...,1]
print(y[...,0])