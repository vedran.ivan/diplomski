import tensorflow as tf
import numpy as np
from scipy.optimize import  linear_sum_assignment




def bbToYolo(bb):

    x1, y1, x2, y2 = np.split(bb, 4, axis=1)
    w = x2 - x1
    h = y2 - y1
    xc = x1 + w / 2.0
    yc = y1 + h / 2.0

    return np.concatenate((xc,yc,w,h), axis=-1)




def compute_iou(y_true, y_pred):
    """
    Calculates IoU (jaccard index)
    Args:
        y_true: [b1, 4] ground truth bounding boxes coordinates [xmin, ymin, xmax, ymax]
        y_pred: [b2, 4] predictor bounding boxes coordinates

    Returns: IOU between ground truth and predictions
        [b1, b2]

    """


    assert y_true.shape[-1] == 4
    assert y_pred.shape[-1] == 4

    iou = np.zeros((y_true.shape[0], y_pred.shape[0]))
    y_true = y_true[:,np.newaxis,:]
    y_pred = y_pred[np.newaxis,:,:]

    xa = np.maximum(y_true[:,:,0], y_pred[:,:,0])
    ya = np.maximum(y_true[:,:,1], y_pred[:,:,1])
    xb = np.minimum(y_true[:,:,2], y_pred[:,:,2])
    yb = np.minimum(y_true[:,:,3], y_pred[:,:,3])

    interArea = np.maximum(0, xb - xa) * np.maximum(0, yb - ya)

    trueArea = (y_true[:,:, 2] - y_true[:,:,0]) * (y_true[:,:, 3] - y_true[:,:,1])
    predArea = (y_pred[:,:, 2] - y_pred[:,:,0]) * (y_pred[:,:, 3] - y_pred[:,:,1])
    unionArea =  trueArea + predArea - interArea

    nonzero = interArea > 0
    if np.any(nonzero):
        nonzero_ind = np.where()
        iou[nonzero_ind] = np.true_divide(interArea[nonzero_ind], unionArea[nonzero_ind])

    return iou




def preprocess_ground_truth(gt, y_preds, img_dim, s):
    '''
    Creates grount truth vector y for loss calculation
    Args:
        gt:   [batch_size,s,s,5,5]
        y_preds: tf.Tensor [batch_size,s,s,B,5]
        img_dim: original image dimension
        s: grid cell size

    Returns: np.array, ground truth vector y

    '''

    y_preds = y_preds.numpy()
    out_shape = y_preds.shape

    #selektor za boxeve u gt koji sadrže objekt (object_selector(i,j,k,m) = 1, ako je m objekt)
    object_selector = np.zeros(out_shape[:4]) #(batch_size,s,s,B)

    y = np.zeros(out_shape) # (batch_size, s, s, B, 5)
    d = img_dim / s

    assert tf.rank(y_preds) == 5
    assert tf.rank(gt) == 5

    for i in range(gt.shape[0]):
        for gy in range(y_preds.shape[1]):
            for gx in range(y_preds.shape[2]):

                #if grid cell contains no object continue to next grid cell
                if not np.any(gt[i,gy,gx,:,4]):
                    continue

                #scale up predictions to image-space ?
                p_xc = y_preds[i,gy,gx,:,0][:,None]
                p_yc = y_preds[i,gy,gx,:,1][:,None]
                p_w = y_preds[i,gy,gx,:,2][:,None] * img_dim
                p_h = y_preds[i,gy,gx,:,3][:,None] * img_dim

                p_xmin = (gx + p_xc) * d - p_w / 2.0
                p_ymin = (gy + p_yc) * d - p_h / 2.0
                p_xmax = (gx + p_xc) * d + p_w / 2.0
                p_ymax = (gy + p_yc) * d + p_h / 2.0

                pred_boxes = np.concatenate((p_xmin, p_ymin, p_xmax, p_ymax), axis=-1) # (?,4)

                iou = compute_iou(gt[i,gy,gx,:,:4], pred_boxes) # B_true x B_pred

                obj_factor = gt[i,gy,gx,:,4].reshape((-1,1))
                iou_matrix = 2.0 - (iou + obj_factor)

                true_ind, pred_ind = linear_sum_assignment(iou_matrix) # find best iou assignment


                # ground truth coord transform to yolo format (xc,yc,w,h)
                true_boxes = bbToYolo(gt[i, gy, gx, :, :4])

                y[i,gy,gx,pred_ind,:4] = true_boxes[true_ind,:] # provjeriti ispravnost
                y[i,gy,gx,pred_ind, 4] = iou[true_ind, pred_ind] # iou


                object_selector[i,gy,gx,pred_ind] = gt[i,gy,gx,true_ind, 4]


    return tf.convert_to_tensor(object_selector, dtype=tf.float32),\
            tf.convert_to_tensor(y, dtype=tf.float32)




class YoloLoss(tf.keras.losses.Loss):

    def __init__(self, img_dim=224, C=0, name="yolo-loss"):
        super().__init__(name=name)
        self.img_dim = tf.constant(img_dim, dtype=tf.float32)
        self.C = tf.constant(C, dtype=tf.int32)


    def call(self, ground_truth, y_preds):
        """

        Args:
            ground_truth: np.array [batch_size, s, s, B, (5+C)]
            y_pred: tf.Tensor [batch_size, s, s, B*(5+C)],  network output

        Returns:
            [batch_size, s, s] batch loss

        """

        s = tf.constant(y_preds.shape[1], dtype=tf.float32)
        d = tf.math.truediv(self.img_dim, s)

        out_shape = y_preds.shape

        y_preds = tf.identity(y_preds)
        y_preds = tf.reshape(y_preds, [out_shape[0], out_shape[1], out_shape[2], -1, 5])

        object_mask, y_true = preprocess_ground_truth(ground_truth, y_preds, self.img_dim, s)


        #### Transform network predictions up to image-scale ####

        pred_xy = y_preds[..., :2]
        cell_inds = tf.range(s, dtype=tf.float32)


        pred_xy = tf.stack((
            pred_xy[...,0] + tf.reshape(cell_inds, [1,1,-1,1]),
            pred_xy[...,1] + tf.reshape(cell_inds, [1,-1,1,1])
        ), axis=-1)

        pred_xy = tf.multiply(pred_xy, d)

        pred_wh = y_preds[..., 2:4] * self.img_dim
        pred_conf = y_preds[...,4]


        #### Process ground truth ####

        true_xy = y_true[..., :2]
        true_wh = y_true[..., 2:4]
        best_iou = y_true[...,4]


        # Maska boxeva kojima nije pridružen objekt
        noobj_mask = 1.0 - object_mask


        #### Calculate loss ####

        xy_sse = tf.math.square(true_xy - pred_xy) * object_mask[..., None] # (batch_size, s, s, B, 2)
        xy_loss = tf.math.reduce_sum(xy_sse, axis=[1,2,3,4])

        wh_sse = tf.math.square(tf.sqrt(true_wh) - tf.sqrt(pred_wh)) * object_mask[..., None] # (batch_size, s, s, B, 2)
        wh_loss = tf.math.reduce_sum(wh_sse, axis=[1,2,3,4])

        obj_conf_sse = tf.math.square(best_iou - pred_conf) * object_mask # (batch_size, s, s, B)
        obj_conf_loss = tf.math.reduce_sum(obj_conf_sse, axis=[1,2,3])

        noobj_conf_sse = tf.math.square(pred_conf) * noobj_mask # * filter za iou < neki prag ??
        noobj_conf_loss = tf.math.reduce_sum(noobj_conf_sse, axis=[1,2,3])


        coord_scale = tf.constant(5.0, dtype=tf.float32)
        noobj_scale = tf.constant(0.5, dtype=tf.float32)

        # vratiti skalar ili batch vektor?

        loss = tf.math.reduce_sum(coord_scale * (xy_loss + wh_loss) + \
                                obj_conf_loss + noobj_scale * noobj_conf_loss)

        return loss


