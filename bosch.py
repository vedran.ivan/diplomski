#!/usr/bin/env python3
"""
Sample script to receive traffic light labels and images
of the Bosch Small Traffic Lights Dataset.

Example usage:
    python bosch.py input_yaml
"""

import os
import yaml

from skimage.io import imread
from skimage.transform import resize, AffineTransform, warp

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
#from bstld.tf_object_detection import constants

B_IMG_DIMS = (720, 1280, 3)


LABEL_TRAIN_PATH = '/home/colt/datasets/bosch_data/train_rgb/train.yaml'


def draw_boxes(X, boxes, s, show_grid=True):
    fig, ax = plt.subplots(1)

    ax.imshow(X)
    ax.set_xticks(np.arange(0, X.shape[1], X.shape[1]/s))
    ax.set_yticks(np.arange(0, X.shape[0], X.shape[0]/s))


    if show_grid:
        plt.grid(color='r', linestyle='-', linewidth=1)

    for b in boxes:
        xc = b[0]
        yc = b[1]
        w = b[2]
        h = b[3]

        x_min = xc - w / 2.0
        y_min = yc - h / 2.0

        rect = patches.Rectangle((x_min, y_min), w, h, linewidth=1, edgecolor='g', facecolor='none')
        ax.add_patch(rect)


    plt.show()



class BoschDataset():

    def __init__(self, label_path, target_whc, IMG_W=1280, IMG_H=720):
        self.label_path = label_path
        self.IMG_W = IMG_W
        self.IMG_H = IMG_H
        self.w, self.h, self.channels = target_whc
        self.x_scale = self.w / self.IMG_W
        self.y_scale = self.h / self.IMG_H
        self.labels = self.process_labels(self.label_path)


    def process_labels(self, input_yaml, riib=False, clip=True):
        """ Gets all labels within label file

        Note that RGB images are 1280x720 and RIIB images are 1280x736.
        Args:
            input_yaml->str: Path to yaml file
            riib->bool: If True, change path to labeled pictures
            clip->bool: If True, clips boxes so they do not go out of image bounds
        Returns: Labels for traffic lights
        """
        assert os.path.isfile(input_yaml), "Input yaml {} does not exist".format(input_yaml)
        with open(input_yaml, 'rb') as iy_handle:
            labels = yaml.load(iy_handle)

        if not labels or not isinstance(labels[0], dict) or 'path' not in labels[0]:
            raise ValueError('Something seems wrong with this label-file: {}'.format(input_yaml))

        #only images with bounding boxes
        labels = [label for label in labels if len(label['boxes']) != 0]


        for i in range(len(labels)):
            labels[i]['path'] = os.path.abspath(os.path.join(os.path.dirname(input_yaml),
                                                             labels[i]['path']))
            # There is (at least) one annotation where xmin > xmax
            for j, box in enumerate(labels[i]['boxes']):
                if box['x_min'] > box['x_max']:
                    labels[i]['boxes'][j]['x_min'], labels[i]['boxes'][j]['x_max'] = (
                        labels[i]['boxes'][j]['x_max'], labels[i]['boxes'][j]['x_min'])
                if box['y_min'] > box['y_max']:
                    labels[i]['boxes'][j]['y_min'], labels[i]['boxes'][j]['y_max'] = (
                        labels[i]['boxes'][j]['y_max'], labels[i]['boxes'][j]['y_min'])

            # There is (at least) one annotation where xmax > 1279
            if clip:
                for j, box in enumerate(labels[i]['boxes']):
                    labels[i]['boxes'][j]['x_min'] = max(min(box['x_min'], self.IMG_W - 1), 0)
                    labels[i]['boxes'][j]['x_max'] = max(min(box['x_max'], self.IMG_W - 1), 0)
                    labels[i]['boxes'][j]['y_min'] = max(min(box['y_min'], self.IMG_H - 1), 0)
                    labels[i]['boxes'][j]['y_max'] = max(min(box['y_max'], self.IMG_H - 1), 0)

                    #scaling
                    labels[i]['boxes'][j]['x_min'] = labels[i]['boxes'][j]['x_min'] * self.x_scale
                    labels[i]['boxes'][j]['x_max'] = labels[i]['boxes'][j]['x_max'] * self.x_scale
                    labels[i]['boxes'][j]['y_min'] = labels[i]['boxes'][j]['y_min'] * self.y_scale
                    labels[i]['boxes'][j]['y_max'] = labels[i]['boxes'][j]['y_max'] * self.y_scale


        return labels

    def get_all_labels(self):
        return self.labels


    def get_image_size(self):
        x = imread(self.labels[0]['path'])
        return x.shape # (height, width, channels)


    def size(self):
        return len(self.labels)


    def getImage(self, i):
        return resize(imread(self.labels[i]['path']), (self.w, self.h))


    def getBoxes(self, i, format='bb'):
        boxes_dicts = self.labels[i]['boxes']

        boxes = []

        assert format in ['bb', 'yolo']

        if format == 'bb':
            boxes = [[box['x_min'], box['y_min'], box['x_max'], box['y_max']] for box in boxes_dicts]

        if format == 'yolo':
            for box in boxes_dicts:
                w = box['x_max'] - box['x_min']
                h = box['y_max'] - box['y_min']
                xc = box['x_min'] + w / 2.0
                yc = box['y_min'] + h / 2.0

                boxes.append([xc,yc,w,h])

        return np.array(boxes)





if __name__ == '__main__':
    """
    if len(sys.argv) < 2:
        print(__doc__)
        sys.exit(-1)
    get_all_labels(sys.argv[1])
    """
    bdata = BoschDataset(LABEL_TRAIN_PATH, (224,224))

    img = bdata.getImage(100)
    boxes = bdata.labels[100]['boxes']
