import numpy as np
from skimage.color import rgb2hsv, hsv2rgb
from skimage.io import imread, imshow, show
from skimage.transform import AffineTransform, warp
import matplotlib.pyplot as plt


rgb_img = imread('/home/colt/datasets/bosch_data/train_rgb/rgb/train/2017-02-03-11-44-56_los_altos_mountain_view_traffic_lights_bag/207468.png')

hsv_img = rgb2hsv(rgb_img)

hsv_img[:,:,1] *= 1.5 #saturation
hsv_img[:,:,2] *= 0.8 #value

rgb_img2 = hsv2rgb(hsv_img)

fig, (ax0, ax1) = plt.subplots(ncols=2, figsize=(8, 2))
ax0.imshow(rgb_img)
ax1.imshow(rgb_img2)
#ax2.imshow()

fig.tight_layout()
show()

#tform = AffineTransform(translation=(-150, 200))
#img_tr = warp(img, tform)