import tensorflow as tf
import numpy as np

from yolomodel import YoloNet
from dataGenerator import ImageDataGenerator
from loss import YoloLoss

from bosch import BoschDataset, LABEL_TRAIN_PATH
bosch_data = BoschDataset(LABEL_TRAIN_PATH, (224,224,3))
bosch_labels = bosch_data.get_all_labels()

datagenerator = ImageDataGenerator(bosch_labels)

yolo_model = YoloNet(input_shape=(224,224,3)).make_model()

train_loss_results = []





def run_train(model, num_epochs=1):

    optimizer = tf.keras.optimizers.Adam(learning_rate=1e-3)
    loss_fn = YoloLoss()
    #loss_metric = tf.keras.metrics.Mean()

    for epoch in range(num_epochs):
        epoch_loss_avg = tf.keras.metrics.Mean()

        for step, (x_batch, y_batch) in enumerate(datagenerator):

            with tf.GradientTape() as tape:

                x_batch = x_batch.astype('float32')
                y_batch = y_batch.astype('float32')

                #get predictions on model
                y_preds = model(x_batch)

                #calculate loss
                loss_value = loss_fn.call(y_batch, y_preds)

                #update running average
                epoch_loss_avg.update_state(loss_value)
                print(loss_value)

            #calculate gradients of loss function w.r.t network weights
            grads = tape.gradient(loss_value, model.trainable_weights)

            optimizer.apply_gradients(zip(grads, model.trainable_weights))

        #end epoch
        train_loss_results.append(epoch_loss_avg.result())

        print("Epoch: %4d, loss=%1.2f" % (epoch, epoch_loss_avg.result()))




run_train(yolo_model, num_epochs=2)