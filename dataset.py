import numpy as np
import tensorflow as tf
from yolo2_loss import processBoundingBoxes
from bosch import BOSCH_DATA
from skimage.io import resize
from skimage.io import imread

class SequenceGen(tf.keras.utils.Sequence):

    def __init__(self, dataset, batch_size, shuffle=True):
        self.dataset = dataset
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        return np.math.ceil(self.dataset.size()/ self.batch_size)

    def __getitem__(self, idx):
        'returns one batch'
        indexes = self.indexes[self.batch_size * idx : self.batch_size * (idx + 1)]
        X_true, y_true = self.__data_generation(indexes)
        return X_true, y_true

    def on_epoch_end(self):
        self.indexes = np.arange(self.dataset.size())
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        X = np.array([self.dataset.getImage(i) for i in list_IDs_temp])
        y = np.array([processBoundingBoxes(self.dataset.getBoxes(i)) for i in list_IDs_temp])

        return X, y


if __name__ == 'main':
    sgen = SequenceGen(BOSCH_DATA, 10)
