import tensorflow as tf

from tensorflow import keras


class Yolo(tf.keras.Model):

    def __init__(self):
        super(Yolo, self).__init__()

        #block 1
        self.conv2d_1 = tf.keras.layers.Conv2D(64, (7,7), strides=(2,2), name='block1_conv2d_1')

        self.leaky_1 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.maxpool_1 = tf.keras.layers.MaxPool2D(pool_size=(2,2), strides=(2,2))

        #block 2
        self.conv2d_2 = tf.keras.layers.Conv2D(192, (3,3), name='block2_conv2d_2')

        self.leaky_2 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.maxpool2d_2 = tf.keras.layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2))

        #block 3
        self.conv2d_3 = tf.keras.layers.Conv2D(128, (1,1), name='block3_conv2d_3')
        self.leaky_3 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_4 = tf.keras.layers.Conv2D(256, (3,3), name='block3_conv2d_4')
        self.leaky_4 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_5 = tf.keras.layers.Conv2D(256, (1,1), name='block3_conv2d_5')
        self.leaky_5 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_6 = tf.keras.layers.Conv2D(512, (3,3), name='block3_conv2d_6')
        self.leaky_6 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.maxpool2d_3 = tf.keras.layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2))

        #block 4
        self.conv2d_7 = tf.keras.layers.Conv2D(256, (1,1), name='block4_conv2d_7')
        self.leaky_7 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_8 = tf.keras.layers.Conv2D(512, (3, 3), name='block4_conv2d_8')
        self.leaky_8 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_9 = tf.keras.layers.Conv2D(256, (1, 1), name='block4_conv2d_9')
        self.leaky_9 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_10 = tf.keras.layers.Conv2D(512, (3, 3), name='block4_conv2d_10')
        self.leaky_10 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_11 = tf.keras.layers.Conv2D(256, (1, 1), name='block4_conv2d_11')
        self.leaky_11 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_12 = tf.keras.layers.Conv2D(512, (3, 3), name='block4_conv2d_12')
        self.leaky_12 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_13 = tf.keras.layers.Conv2D(256, (1, 1), name='block4_conv2d_13')
        self.leaky_13 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_14 = tf.keras.layers.Conv2D(512, (3, 3), name='block4_conv2d_14')
        self.leaky_14 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_15 = tf.keras.layers.Conv2D(512, (1,1), name='block4_conv2d_15')
        self.leaky_15 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_16 = tf.keras.layers.Conv2D(1024, (3, 3), name='block4_conv2d_16')
        self.leaky_16 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.maxpool2d_4 = tf.keras.layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2))

        #block 5
        self.conv2d_17 = tf.keras.layers.Conv2D(512, (1,1), name = 'block_conv2d_17')
        self.leaky_17 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_18 = tf.keras.layers.Conv2D(1024, (3,3), name = 'block_conv2d_18')
        self.leaky_18 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_19 = tf.keras.layers.Conv2D(512, (1, 1), name='block_conv2d_19')
        self.leaky_19 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_20 = tf.keras.layers.Conv2D(1024, (3, 3), name='block_conv2d_20')
        self.leaky_20 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_21 = tf.keras.layers.Conv2D(1024, (3, 3), name='block_conv2d_21')
        self.leaky_21 = tf.keras.layers.LeakyReLU(alpha=0.1)

        self.conv2d_22 = tf.keras.layers.Conv2D(1024, (3, 3), strides=(2,2), name='block_conv2d_22')
        self.leaky_22 = tf.keras.layers.LeakyReLU(alpha=0.1)

    def call(self, inputs):
        pass

        #define loss
        #self.add_loss(my_loss)


'''
def train_loop():
    pass
    
def train():
    pass 
    
def evaluate():
    pass
'''