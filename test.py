import tensorflow as tf
import numpy as np

from bosch import BoschDataset,LABEL_TRAIN_PATH
from dataGenerator import ImageDataGenerator
from yolomodel import YoloNet
from bosch import draw_boxes
from loss import compute_iou, preprocess_ground_truth, bbToYolo



#a = np.array([[1.0,3.5,7.0,9.0],[1.5,4.5,4.0,8.5],[4.5,0.5,7.0,2.0]])
#b = np.array([[2.0,4.0,4.5,8.0],[5.0,4.0,6.5,8.0],[6.0,1.5,8.0,3.0]])

img_size = (480, 480, 3)


#### DEFINIRANJE DATASETA I GENERATORA BATCHEVA ####
print("Učitavanje dataseta\n")
bdata = BoschDataset(LABEL_TRAIN_PATH, img_size)
labels = bdata.get_all_labels()

datagen = ImageDataGenerator(labels, image_size=img_size)



#### GENERIRANJE PODATAKA ZA TRENING ####
print("\nGeneriranje batcha \n")

X,gt = datagen.__getitem__(0)



#### TESTIRANJE MREŽE ####
print("\nUnaprijedni prolaz kroz mrežu \n")

net = YoloNet(input_shape=img_size, n_bounding_boxes=2, n_cells=7, n_classes=0)
yolo_model = net.buildModel()
y_preds = yolo_model(X)
pred_shape = tf.shape(y_preds)

y_preds = tf.reshape(y_preds, [pred_shape[0],pred_shape[1],pred_shape[2], -1, 5])
pred_boxes = tf.reshape(y_preds[0,:,:,:], [-1,5])
pred_boxes = pred_boxes[:,:4]

print(pred_boxes)

#### PROCESIRANJE GROUND TRUTH LABELA ####
print("\nPreprocesiranje ground truth labela \n")
#y = preprocess_ground_truth(gt,y_preds, 224, 7)
#print(y)


#### ISPIS PARAMETARA MREŽE ####
yolo_model.summary()
