import tensorflow as tf
import numpy as np

C = 0 # number of classes


"""
priors: (B, 2) Bounding box priors width, height 



"""

def loss_fn(y, y_hat, box_iou):
    """

    Args:
        y (np.ndarray):  m x s x s x d array. targets
        y_hat (np.ndarray): m x s x s x d array. predictions
        box_iou: m x s x s x b array. 0 if no object in the box, max IOU otherwise

    Returns:

    """
    m,s,s,d = y
    b = d // (4 + 1 + C)
    y_reshape = y[:,:,:,b,-1]

    obj_ind = box_iou > 0



def sigmoid(x):
    return 1. / (1 + np.exp(-x))

def get_grid_indices(boxes, cell_size):
    """
    For each box, calculates the grid cell responsible for predicting object in that box

    Args:
        boxes (np.ndarray, ndim=4): array with last dimension representing box center coordinates, width, and height [xc, yc, w, h]

        cell_size (int): size of grid cell

    Returns:
        (np.ndarray, ndim=2) grid coordinates responsible for predicting boxes

    """
    return boxes[...,0:2] // cell_size

def get_cell_offsets(s, cell_size=32):
    x = np.arange(s)[np.newaxis,:]
    xx = np.tile(x.T, (1, s))
    yy = np.tile(x, (s,1))
    z = np.stack((xx, yy),axis=-1) * cell_size
    return z


def transform_coords(coords, inp, out ):
    """
    Transform between box coordinate encodings

    'diag': [xmin, ymin, xmax, ymax]
    'center': [xc, yc, w, h]
    'min': [xmin, ymin, w, h]

    Args:
        coords (np.ndarray): coordinates to transform
        inp (string): one of ['diag', 'center', 'min']
        out: one of ['diag', 'center', 'min']

    Returns:

    """


    diag_center = np.array([[0.5, 0   , -1,  0],
                            [0  , 0.5 ,  0,  1],
                            [0.5, 0   ,  1,  0],
                            [0  , 0.5 ,  0,  1]], dtype=np.float32)

    min_center = np.array([[1,  0,  0,  0],
                           [0,  1,  0,  0],
                           [0.5,0,  1,  0],
                           [0,  0.5,0,  1]], dtype=np.float32)

    center_diag = np.array([[1,     0,     1,     0],
                            [0,     1,     0,     1],
                            [-0.5,  0,     0.5,   0],
                            [0,     -0.5,  0,     0.5]], dtype=np.float32)

    center_min = np.array([[1,      0,       0,   0],
                           [0,      1,       0,   0],
                           [-0.5,   0,       1,   0],
                           [0,      -0.5,    0,   1]], dtype=np.float32)

    transform_inp = {'diag':diag_center, 'center': None, 'min':min_center}
    transform_out = {'diag':center_diag, 'center': None, 'min':center_min}

    t1_mat = transform_inp[inp]
    t2_mat = transform_out[out]

    transformed = coords

    if t1_mat is not None:
        transformed = np.dot(transformed, t1_mat)
        print("a")
    if t2_mat is not None:
        transformed = np.dot(transformed, t2_mat)
        print(t2_mat)

    return transformed


def offset_predictions(t, cell_offsets, bbox_priors):
    """

    Args:
        t (np.ndarray, ndim=4): Network predictions, [tx, ty, tw, th, c]
        cell_offsets (np.ndarray, ndim=3):  Offsets (cx, cy) from the top left corner of the image for each cell
        bbox_priors (np.ndarray, ndim=2): b x 4 array. Bounding box priors [px, py, pw, ph]

    Returns (np.ndarray): m x s x s x d array. Predictions offset by grid cell position.

    """
    m,s,s,d = t.shape
    b,_ = bbox_priors.shape

    t = t.reshape((m,s,s,b,-1)) # last dimension contains coordinates and class probs
    offset_boxes = np.zeros_like(t)

    offset_boxes[:, :, :, :, 0:2] = sigmoid(t[:, :, :, :, 0:2]) + cell_offsets[:, :, np.newaxis ,:]     # bx = sigmoid(tx) + cx; by = sigmoid(ty) + cy
    offset_boxes[:, :, :, :, 2:4] = np.exp(t[:, :, :, :, 2:4]) * bbox_priors[:, 2:4]                    #  bw = pw * exp(tw); bh = ph * exp(th)
    offset_boxes[:, :, :, :, 4] = sigmoid(t[:, :, :, :, 4])                                             # Pr(Object) * IOU(b, object) = sigmoid(to)

    return offset_boxes.reshape((m,s,s,d))


def correct_predictions(ground_truth, y_hat, cell_size, feature_size=5):
    #ground_truth [m, n, 4]
    #y_hat [m, s, s, b*feature_size]

    #note1: ground_truth must use center-box coordinates
    m,n,_ = ground_truth.shape
    _,s,_,d = y_hat.shape
    b = d // feature_size


    pred_boxes = y_hat.reshape((m, s, s, b, feature_size))[...,0:4] # grab box coordinates
    pred_boxes = transform_coords(pred_boxes, inp='min', out='diag') # uses diagonal coordinates of boxes

    grid_ind = get_grid_indices(transform_coords(ground_truth, inp='min', out='center'), cell_size) # m x n x 2
    obj_box_iou = np.zeros(m, s, s, b)
    y = np.zeros_like(y_hat)

    for k in range(m):
        for j in range(n):

            cell_x = grid_ind[k,j,0]
            cell_y = grid_ind[k,j,1]

            truth_box = transform_coords(ground_truth[k,j,:], inp='min', out='diag') # 4

            iou = IOU(truth_box, pred_boxes[k, cell_x, cell_y,:,:]) # 1 x b
            max_iou_ind = np.argmax(iou) # index of box having max(iou(truth, box))
            max_iou = iou[max_iou_ind]

            if obj_box_iou[k, cell_x, cell_y, max_iou_ind] < max_iou:
                y[k, cell_x, cell_y, max_iou_ind * feature_size: max_iou_ind * feature_size + 4] = truth_box
                y[k, cell_x, cell_y, max_iou_ind * feature_size + 4] = max_iou # confidence
                obj_box_iou[k, cell_x, cell_y, max_iou_ind] = max_iou

    return y, obj_box_iou


def get_cells_predictions(cell_inds, output):

    assert cell_inds.dim in [1,2]
    assert output.dim == 3
    assert output.shape[0] == output.shape[1]
    if cell_inds.dim == 2:
        assert cell_inds.shape[-1] == 2

    if cell_inds.dim == 1:
        return output[cell_inds[0], cell_inds[1], :]

    else:
        return output[cell_inds[:,0], cell_inds[:,1], :]


def post_processing():
    #offset_predictions
    #correct_predictions
    #loss...

    pass


def IOU(a,b):
    """
    Calculate IOU (intersection over union), i.e. Jaccard overlap between two sets of boxes, A and B.
    Sets A and B can have different cardinalities.
    Uses top-left coordinate origin in image space.
    Uses [xmin, ymin, xmax, ymax] coordinate encoding.

    Args:
        a (np.ndarray, ndim <= 2): set A containing n boxes
        b (np.ndarray, ndim <= 2): set B containing m boxes

    Returns (np.ndarray): n x m matrix. Last dimension is IOU between a box in A and all the boxes in B.

    """
    assert a.ndim in [1,2], "a must have at most 2 dimensions"
    assert b.ndim in [1,2], "b must have at most 2 dimensions"
    assert b.shape[-1] == 4, "last dimension of a must have size of 4."
    assert a.shape[-1] == 4, "last dimension of a must have size of 4."

    if a.ndim == 1:
        a = a[np.newaxis,:]
    if b.ndim == 1:
        b = b[np.newaxis,:]

    aa = a[:,np.newaxis]
    bb = b[np.newaxis]

    aa = np.tile(aa, (1, b.shape[0], 1))
    bb = np.tile(bb, (a.shape[0], 1, 1))

    z = np.stack((aa, bb), axis=-1)

    Sa = (z[:,:,2,0] - z[:,:,0,0]) * (z[:,:,3,0] - z[:,:,1,0])
    Sb = (z[:,:,2,1] - z[:,:,0,1]) * (z[:,:,3,1] - z[:,:,1,1])

    ab_w = np.maximum(0, np.minimum(z[:,:,2,0], z[:,:,2,1]) - np.maximum(z[:,:,0,0], z[:,:,0,1]))
    ab_h = np.maximum(0, np.minimum(z[:,:,3,0], z[:,:,3,1]) - np.maximum(z[:,:,1,0], z[:,:,1,1]))

    aIb = ab_w * ab_h
    aUb = Sa + Sb - aIb

    iou = aIb / aUb

    return iou


def input_label_offset(bbox_labels, img_dim, cell_dim):
    """
    Turns bounding box coordinates in the image space to offsets in range [0,1] relative to grid cell

    :param bbox_labels: np.ndarray, ndim=2 (?,4)
        label bounding box coordinates 0 < lx, lw <= img_width; 0 < ly,lh <= img_height
    :param cell_dim: integer
        cell dimension
    :param img_dim: integer
        image dimension
    :return: np.ndarray, ndim=2 (?, 6) (bbox, (cx, cy, bx, by, bw, bh))
    """

    # (n, (tx, ty, tw, th))
    # (n, (bx, by, bw, bh))
    bbox_normalized = np.zeros(bbox_labels.shape, dtype=np.float32)
    target_cells = np.zeros((bbox_labels.shape[0], 2), dtype=np.int)
    target_cells[:,0] = bbox_labels[:,0] // cell_dim
    target_cells[:,1] = bbox_labels[:,1] // cell_dim
    bbox_normalized[:,0] = bbox_labels[:,0] / cell_dim - target_cells[:,0]
    bbox_normalized[:,1] = bbox_labels[:,1] / cell_dim - target_cells[:,1]
    bbox_normalized[:,2] = bbox_labels[:,2] / img_dim
    bbox_normalized[:,3] = bbox_labels[:,3] / img_dim
    return bbox_normalized, target_cells


truth = np.array([60, 50, 40, 60])
preds = np.array([[35, 25, 30, 30], [21,70,30,20], [75, 20, 30, 20], [70, 60, 20, 40], [70, 75, 40, 30], [60, 50, 40, 60]])
xx = np.array([[20, 10, 30, 30,], [ 6, 60, 30, 20,], [60, 10, 30, 20,], [60, 40, 20, 40,], [50, 60, 40, 30,], [40, 20, 40, 60]])


m = 1
s = 2
b = 2

x = np.arange(40).reshape((1,s,s,10))
y = np.arange(8).reshape((s,s,2))
z = np.arange(8).reshape((b,4))

print(transform_coords(np.array([20,20,40,40]), inp='min', out='center'))